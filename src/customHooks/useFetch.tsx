import React, { useState, useEffect } from "react";

interface ParametersI {
  accessKey?: string;
  query?: string;
}

export const useFetch = (
  url: string,
  loadNext: boolean,
  options: object | undefined,
  parameters: ParametersI,
  backupData?: any
) => {
  const [response, setresponse] = useState();
  const { accessKey, query } = parameters;

  const getAPI = async () => {
    try {
      console.log(url + (accessKey ? accessKey : "") + (query ? query : ""));
      let fetchedData = await fetch(
        url + (accessKey ? accessKey : "") + (query ? query : ""),
        { ...options }
      );
      let fetchJSON = await fetchedData.json();
      console.log(fetchedData);
      console.log(fetchJSON);
      setresponse(fetchJSON);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getAPI();
  }, [loadNext]);

  if (response) return response as any;
  else return backupData;
};
