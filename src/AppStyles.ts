import styled from "styled-components";

export const StyledApp = styled.div`
  position: relative;
  width: 100vw;
  min-height: 100vh;
  height: 100%;
  box-sizing: border-box;
  background: var(--dark-blue);

  display: grid;
  grid-template-rows: 60px auto;
`;

export const AppContent = styled.div`
  grid-row: 2/2;
  height: 100%;
  width: 80%;
  display: flex;
  position: relative;
  justify-self: center;
  justify-content: center;
`;
