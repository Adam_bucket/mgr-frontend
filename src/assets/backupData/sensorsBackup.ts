const sensorsBackup = [
  {
    id: 1,
    name: "temperature",
    value: "100",
    unit: "C",
  },
  {
    id: 2,
    name: "sound",
    value: "100",
    unit: "%",
  },
  {
    id: 3,
    name: "lightness",
    value: "100",
    unit: "L",
  },
  {
    id: 4,
    name: "vibrations",
    value: "100",
    unit: "Ampl",
  },
];

export default sensorsBackup;
