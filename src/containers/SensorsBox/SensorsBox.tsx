import { useEffect, useState } from "react";
import {
  IdSubtile,
  NameSubtile,
  SensorTile,
  SensorTileWrapper,
  StyledSensorsBox,
  UnitSubtile,
  ValueSubtile,
} from "./SensorsBoxStyles";
import { ReactComponent as IconTemperature } from "../../assets/icons/icon-temperature.svg";
import { ReactComponent as IconLightness } from "../../assets/icons/icon-lightness.svg";
import { ReactComponent as IconVibrations } from "../../assets/icons/icon-vibrations.svg";
import { ReactComponent as IconSound } from "../../assets/icons/icon-sound.svg";

export interface SensorI {
  name: string;
  value: String;
  id: number;
  unit: number;
}

export interface SensorsBoxI {
  sensorsApi: Array<SensorI>;
}
// points.sort(function (a, b) {
//   return b - a;
// });

const SensorsBox = ({ sensorsApi }: SensorsBoxI) => {
  const [sortedSensors, setSortedSensors] = useState(sensorsApi || []);
  const iconArray = [
    <IconTemperature />,
    <IconSound />,
    <IconLightness />,
    <IconVibrations />,
  ];

  useEffect(() => {
    setSortedSensors(sensorsApi.sort((a, b) => a.id - b.id));
    console.log(sortedSensors);
  }, [sensorsApi]);
  return (
    <StyledSensorsBox numberOfSensors={sortedSensors.length}>
      {sortedSensors.map(({ name, value, id, unit }) => {
        return (
          <SensorTileWrapper>
            {iconArray[id - 1]}
            <SensorTile>
              <NameSubtile>{name}</NameSubtile>
              <ValueSubtile>
                {value.concat(" ").concat(unit.toString())}
              </ValueSubtile>
              {/* <IdSubtile>{id}</IdSubtile> */}
            </SensorTile>
          </SensorTileWrapper>
        );
      })}
    </StyledSensorsBox>
  );
};

export default SensorsBox;
