import styled from "styled-components";

export interface StyledSensorsBoxI {
  numberOfSensors: number;
}

export const SensorTile = styled.div`
  box-sizing: border-box;
  /* display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column; */
  width: 35%;
  font-size: 20px;
  color: var(--light-blue);
  display: grid;
  grid-template-rows: auto;
  border-radius: 5px;

  * {
    text-align: center;
    width: 100%;
  }
`;

export const SensorTileWrapper = styled.li`
  list-style-type: none;
  box-sizing: border-box;
  display: flex;
  flex-direction: row-reverse;
  width: 50%;
  height: 250px;
  align-items: center;
  justify-content: center;
  svg {
    height: 70%;
    fill: var(--red-reduced);
    margin-left: 10px;
  }
  path {
    fill: var(--red-reduced);
  }
`;

export const StyledSensorsBox = styled.ul<StyledSensorsBoxI>`
  width: 700px;
  min-width: 560px;
  padding: 0;
  margin-top: 90px;
  height: fit-content;
  display: flex;
  flex-wrap: wrap;

  /* ${(props) =>
    `grid-template-rows: repeat(`
      .concat((props.numberOfSensors + 1).toString())
      .concat(`,1fr)`)}; */

  ${SensorTileWrapper}:nth-child(odd) {
    border-right: 2px dashed var(--blue);
    border-bottom: 2px dashed var(--blue);
  }

  ${SensorTileWrapper}:nth-child(even) {
    border-right: none;
    border-bottom: 2px dashed var(--blue);
  }

  ${SensorTileWrapper}:nth-last-child(1) {
    border-right: none;
    border-bottom: none;
  }

  ${SensorTileWrapper}:nth-last-child(2) {
    border-bottom: none;
  }
`;

export const IdSubtile = styled.div`
  background: var(--blue-reduced);
  color: var(--light-blue);
`;

export const NameSubtile = styled.div`
  background: var(--red-reduced);
  color: var(--white);
  border-radius: 5px;
`;

export const ValueSubtile = styled.div``;

export const UnitSubtile = styled.div``;
