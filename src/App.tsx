import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { AppContent, StyledApp } from "./AppStyles";
import Navbar from "./components/navbar/Navbar";
import SensorHistory from "./pages/SensorHistory";
import Home from "./pages/Home";
import Sensors from "./pages/Sensors";

function App() {
  return (
    <StyledApp>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <Router>
        <Navbar />
        <AppContent>
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="/sensors" element={<Sensors />}></Route>
            <Route path="/sensors/:name" element={<SensorHistory />}></Route>
          </Routes>
        </AppContent>
      </Router>
    </StyledApp>
  );
}

export default App;
