import React, { useEffect, useState } from "react";
import urls from "../assets/urls";
import { useFetch } from "../customHooks/useFetch";

interface HomeI {}

interface SensorI {
  name: string;
  value: String;
}

const sensorsOptions = {
  method: "GET",
  mode: "cors",
  headers: {
    "content-type": "application/json",
    accept: "*/*",
  },
  parameters: {
    query: "/get_sensors",
  },
};

const Home = ({}: HomeI) => {
  const [loadNext, setLoadNext] = useState(false);
  const sensorsApi = useFetch(
    urls.springIp,
    loadNext,
    sensorsOptions,
    sensorsOptions.parameters
  );

  useEffect(() => {
    setTimeout(() => {
      setLoadNext(!loadNext);
    }, 100);
  }, [loadNext]);

  return (
    <div style={{ height: "2000px" }}>
      <button>CLICK</button>
      {sensorsApi &&
        sensorsApi.map((sensor: SensorI) => {
          return (
            <>
              <h1>{sensor.name}</h1>
              <h2>{sensor.value}</h2>
            </>
          );
        })}
    </div>
  );
};

export default Home;
