import React, { useEffect, useState } from "react";
import sensorsBackup from "../assets/backupData/sensorsBackup";
import urls from "../assets/urls";
import SensorsBox, { SensorI } from "../containers/SensorsBox/SensorsBox";
import { useFetch } from "../customHooks/useFetch";

interface SensorsI {}

const sensorsOptions = {
  method: "GET",
  mode: "cors",
  headers: {
    "content-type": "application/json",
    accept: "*/*",
  },
  parameters: {
    query: "/get_sensors",
  },
};

const Sensors = ({}: SensorsI) => {
  const [loadNext, setLoadNext] = useState(false);
  const sensorsApi = useFetch(
    urls.springIp,
    loadNext,
    sensorsOptions,
    sensorsOptions.parameters,
    sensorsBackup
  );

  useEffect(() => {
    setTimeout(() => {
      setLoadNext(!loadNext);
    }, 100);
  }, [loadNext]);

  return (
    <>
      {sensorsApi && <SensorsBox sensorsApi={sensorsApi as Array<SensorI>} />}
    </>
  );
};

export default Sensors;
