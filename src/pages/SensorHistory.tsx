import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import sensorBackup from "../assets/backupData/temperatureBackup";
import urls from "../assets/urls";
import SensorGraph from "../components/sensorGraph/SensorGraph";
import SensorTable from "../components/sensorTable/SensorTable";
import { useFetch } from "../customHooks/useFetch";
import { StyledSensorHistory } from "./SensorHistoryStyles";

const SensorHistory = () => {
  const { name } = useParams();

  const sensorOptions = {
    method: "GET",
    mode: "cors",
    headers: {
      "content-type": "application/json",
      accept: "*/*",
    },
    parameters: {
      query: `/${name}_history`,
    },
  };

  const [loadNext, setLoadNext] = useState(false);
  const sensorData = useFetch(
    urls.springIp,
    loadNext,
    sensorOptions,
    sensorOptions.parameters,
    sensorBackup
  );

  useEffect(() => {
    setLoadNext(!loadNext);
  }, [name]);

  useEffect(() => {
    setTimeout(() => {
      setLoadNext(!loadNext);
    }, 5 * 1000);
  }, [loadNext]);

  return (
    <StyledSensorHistory>
      <SensorGraph sensorData={sensorData} title={name} />
      <SensorTable sensorData={sensorData} />
    </StyledSensorHistory>
  );
};

export default SensorHistory;
