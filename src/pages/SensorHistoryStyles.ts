import styled from "styled-components";

export const StyledSensorHistory = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding-top: 50px;
`;
