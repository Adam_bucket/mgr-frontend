import styled from "styled-components";

export interface StyledSensorTableI {
  numberOfData: number;
}

export const SensorLi = styled.li`
  list-style-type: none;
  box-sizing: border-box;
  width: 100%;
  display: grid;
  grid-template-columns: 15% 30% 30% auto;

  * {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const StyledSensorTable = styled.ul<StyledSensorTableI>`
  display: grid;
  width: 60%;
  min-width: 560px;
  padding: 0;
  margin-top: 75px;

  ${(props) =>
    `grid-template-rows: repeat(`
      .concat((props.numberOfData + 1).toString())
      .concat(`,1fr)`)};

  ${SensorLi}:nth-child(even) {
    background: var(--dark-blue);
    color: var(--light-blue);
    font-weight: 500;
    /* border-left: 2px solid var(--light-blue-reduced);
    border-right: 2px solid var(--light-blue-reduced); */
  }

  ${SensorLi}:nth-child(odd) {
    background: var(--blue-reduced);
    color: var(--light-blue);
    font-weight: 500;
  }

  ${SensorLi} {
    transition: 0.1s ease-in-out;
    :hover {
      background: var(--white);
      color: var(--dark-blue);
    }
    * {
      padding: 5px 10px;
    }
    cursor: crosshair;
  }

  ${SensorLi}:nth-child(1) {
    box-sizing: border-box;
    background: var(--red-reduced);
    color: var(--white);
    // border: var(--red-reduced) 2px solid;
    border-radius: 5px 5px 0 0;
    font-weight: 600;
  }
  border-radius: 5px;
`;

export const SensorHistoryId = styled.div``;

export const SensorDate = styled.div``;

export const SensorValue = styled.div``;

export const SensorTime = styled.div``;
