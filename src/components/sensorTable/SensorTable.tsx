import React from "react";
import {
  StyledSensorTable,
  SensorLi,
  SensorDate,
  SensorTime,
  SensorValue,
  SensorHistoryId,
} from "./SensorTableStyles";

export interface SensorDataI {
  date: string; //TODO CAHNGE
  value: number;
  time: number;
  id: number;
}

export interface SensorTableI {
  sensorData: Array<SensorDataI>;
}

const SensorTable = ({ sensorData }: SensorTableI) => {
  return (
    <StyledSensorTable numberOfData={sensorData.length}>
      <SensorLi>
        <SensorHistoryId>{"#"}</SensorHistoryId>
        <SensorDate>{"DATE"}</SensorDate>
        <SensorValue>{"TIME PASSED"}</SensorValue>
        <SensorTime>{"VALUE"}</SensorTime>
      </SensorLi>
      {sensorData.length &&
        sensorData.map(({ id, date, value, time }) => {
          return (
            <SensorLi>
              <SensorHistoryId>{id}</SensorHistoryId>
              <SensorDate>{date}</SensorDate>
              <SensorValue>{time}</SensorValue>
              <SensorTime>{value}</SensorTime>
            </SensorLi>
          );
        })}
    </StyledSensorTable>
  );
};

export default SensorTable;
