import styled from "styled-components";

export const ChartTitle = styled.h1`
  color: var(--light-blue);
`;

export const DivideLine = styled.div`
  background: linear-gradient();
`;

export const ChartWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;
