import React, { useEffect, useState } from "react";
import {
  ResponsiveContainer,
  AreaChart,
  XAxis,
  YAxis,
  Area,
  Tooltip,
  CartesianGrid,
} from "recharts";
import { SensorDataI, SensorTableI } from "../sensorTable/SensorTable";
import CustomTooltip from "./customTooltip/CustomTooltip";
import { StyledCustomTooltip } from "./customTooltip/CustomTooltipStyles";
import { ChartTitle, ChartWrapper, DivideLine } from "./SensorGraphStyles";

//data.push
const SensorGraph = ({
  sensorData,
  title,
}: {
  sensorData: any;
  title: string | undefined;
}) => {
  // const [myData, setMyData] = useState([] as Array<any>);
  var data: { time: number; value: number }[] = [];
  useEffect(() => {
    for (let i = 0; i < 30; i++)
      data.push({
        time: i,
        value: Number.parseFloat((Math.random() * 15).toFixed(2)),
      });
    console.log(data);
  }, []);
  return (
    <ChartWrapper>
      <ChartTitle>
        {title!.charAt(0).toUpperCase() + title!.slice(1)}
      </ChartTitle>
      <DivideLine />
      <ResponsiveContainer width="100%" height={400}>
        <AreaChart data={sensorData}>
          <defs>
            <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
              <stop offset="0%" stopColor="var(--red)" stopOpacity={0.4} />
              <stop
                offset="75%"
                stopColor="var(--light-red)"
                stopOpacity={0.05}
              />
            </linearGradient>
          </defs>
          <Area dataKey="value" fill="url(#color)" />
          <XAxis
            dataKey="time"
            name="Time"
            tickFormatter={(number) => `${number.toFixed(1)}s`}
          />
          <YAxis
            dataKey="value"
            tickCount={10}
            tickFormatter={(number) => `${number.toFixed(1)}C`}
            tickLine={false}
            domain={[0, "auto"]}
            allowDataOverflow={true}
          />
          {/* {data && <Tooltip content={CustomTooltip} />} */}
          {/* <Tooltip contentStyle={{ borderRadius: "0.25rem",
  background: var(--light-grey),
  color: var(--white),
  padding: "1rem",
  box-shadow: 15px 30px 40px 5px var(--dark-grey);
  text-align: center;
  z-index: 301; }} /> */}

          <Tooltip
            contentStyle={{
              background: "var(--dark-grey)",
              opacity: "0.9",
              borderRadius: "0.25rem",
              color: "var(--light-blue)",
              fontWeight: 600,
              padding: "1rem",
              boxShadow: "5px 3px 4px 5px var(--dark-blue)",
              // textAlign: "center",
              zIndex: 301,
            }}
          />
          {/* <Tooltip /> */}
          <CartesianGrid opacity="10%" />
        </AreaChart>
      </ResponsiveContainer>
    </ChartWrapper>
  );
};

export default SensorGraph;
