import styled from "styled-components";

export const StyledCustomTooltip = styled.div`
  border-radius: 0.25rem;
  background: var(--light-grey);
  color: var(--white);
  padding: 1rem;
  box-shadow: 15px 30px 40px 5px var(--dark-grey);
  text-align: center;
  z-index: 301;
`;
