import React, { useEffect } from "react";
import { ContentType } from "recharts/types/component/DefaultLegendContent";
import {
  ValueType,
  NameType,
} from "recharts/types/component/DefaultTooltipContent";
import { StyledCustomTooltip } from "./CustomTooltipStyles";

const CustomTooltip = (props: any) => {
  useEffect(() => {
    setTimeout(() => console.log(props), 5000);
  }, []);

  const CT = (
    <StyledCustomTooltip>
      <h4>{props.label}</h4>
      <p>{props.value} C</p>
    </StyledCustomTooltip>
  );

  return props.active ? CT : null;
};

export default CustomTooltip;
