import React from "react";
import { Link } from "react-router-dom";
import {
  DropDown,
  DropDownContent,
  LinkWrapper,
  NavbarSection,
  StyledNavbar,
} from "./NavbarStyles";
import { ReactComponent as IconSensor } from "../../assets/icons/icon-sensor.svg";
import { ReactComponent as IconRobot } from "../../assets/icons/icon-robot.svg";
import { ReactComponent as IconMap } from "../../assets/icons/icon-map.svg";
import { ReactComponent as IconGeneral } from "../../assets/icons/icon-general.svg";

const sections = [
  {
    text: "Sensors",
    link: "/sensors",
    icon: <IconSensor />,
    dropdowns: [
      { dropText: "MOCKED", dropLink: "/sensors" },
      { dropText: "TEMPERATURE", dropLink: "/sensors/temperature" },
      { dropText: "LIGHTNESS", dropLink: "/sensors/lightness" },
      { dropText: "SOUND INTENSITY", dropLink: "/sensors/sound" },
    ],
  },
  {
    text: "Robot",
    link: "/page2",
    icon: <IconRobot />,
    dropdowns: [],
  },
  {
    text: "Map",
    link: "/page3",
    icon: <IconMap />,
    dropdowns: [],
  },
  {
    text: "General",
    link: "/page4",
    icon: <IconGeneral />,
    dropdowns: [],
  },
];

const Navbar = () => {
  return (
    <StyledNavbar>
      {sections.map(({ link, text, icon, dropdowns }) => {
        return (
          <LinkWrapper>
            <Link to={link} style={{ textDecoration: "none" }}>
              <NavbarSection>
                <h3>{text}</h3>
                {icon}
              </NavbarSection>
              <DropDown>
                {dropdowns.map(({ dropLink, dropText }) => {
                  return (
                    <Link to={dropLink} style={{ textDecoration: "none" }}>
                      <DropDownContent>{dropText}</DropDownContent>
                    </Link>
                  );
                })}
              </DropDown>
            </Link>
          </LinkWrapper>
        );
      })}
    </StyledNavbar>
  );
};

export default Navbar;
