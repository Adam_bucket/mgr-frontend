import styled from "styled-components";

export const DropDown = styled.div`
  box-sizing: border-box;
  display: none;
  width: 100%;
  background: transparent;
  position: relative;

  :hover {
    display: flex;
    flex-direction: column;
  }
`;

export const DropDownContent = styled.div`
  box-sizing: border-box;
  border-bottom: 1px var(--light-blue) groove;
  padding: 3px 3px;
  color: var(--light-blue);

  transition: all 0.2s ease-in-out;
  transition-property: background-color color font-weight;
  :hover {
    background-color: var(--light-blue);
    color: var(--dark-blue);
    font-weight: 700;
  }
`;

// export const LinkWrapper = styled.div`
//   :hover {
//     + ${DropDown} {
//       display: inline-block;
//     }
//   }
// `;

export const NavbarSection = styled.div`
  box-sizing: border-box;
  position: relative;
  height: 100%;
  width: 200px;
  border-right: var(--blue) solid 3px;
  display: flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  font-size: 20px;
  text-decoration: none !important;
  color: var(--light-blue);

  h3 {
    margin-right: 10px;
  }

  svg {
    height: 90%;
  }
  path {
    fill: var(--light-blue);
  }

  transition: 0.2s ease-in-out;
  :hover {
    background-color: var(--light-blue);
    color: var(--dark-blue);
    font-size: 23px;
    svg {
      height: 100%;
    }
    path {
      fill: var(--dark-blue);
    }

    + ${DropDown} {
      transition: 1s ease-in-out;
      display: flex;
      flex-direction: column;
    }
  }
`;

export const LinkWrapper = styled.div``;

export const StyledNavbar = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 60px;
  z-index: 1001;
  background: var(--dark-blue-reduced);

  display: flex;
  flex-direction: row;
  justify-content: center;

  ${LinkWrapper}:nth-child(1) {
    border-left: var(--blue) solid 3px;
  }
`;
